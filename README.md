# BTL-IT3190
----------------
# Spam mail classification using Naive Bayes classification

A miniaturized spam classifier which classifies a given input email as SPAM/NOT-SPAM(HAM). Naive Bayes is used to build the classifier. The dataset used for this project is: [SpamAssassin public mail corpus](https://spamassassin.apache.org/old/publiccorpus)

# Dataset

The dataset was splitted into 3 parts:

* Spam: 3793 spam emails, 40 percents of these emails contain HTML.
* Easy ham: 6451 non-spam emails, quite easy to differentiate from spam.
* Hard spam: 500 non-spam emails, 50 percents of these emails contain HTML and closer to spam in many respects: use HTML, "spammish-sounding" phrases, etc

# Pre-processing

* Strip email header.
* Lower-casing.
* Strip html symbols (ex: `&nbsp;`,etc)
* Strip html tags.
* Strip urls.
* Strip email address.
* Strip punctuation.
* Strip numeric.
* Strip multiple whitespaces.
* Remove short words.
* Remove stopwords.
* Word stemming using Porter Stemmer

Library used: [gensim](https://radimrehurek.com/gensim)

A sample email:

> From mortgage_quotes_fast@nationwidemortgage.us  Sun Sep 22 14:13:24 2002
> 
> Return-Path: \<mortgage_quotes_fast@nationwidemortgage.us>
> 
> Delivered-To: zzzz@localhost.jmason.org
> 
> Received: from localhost (jalapeno [127.0.0.1])
> 	by zzzzason.org (Postfix) with ESMTP id AE85B16F03
> 	for \<zzzz@localhost>; Sun, 22 Sep 2002 14:13:22 +0100 (IST)
> 
> Received: from jalapeno [127.0.0.1]
> 	by localhost with IMAP (fetchmail-5.9.0)
> 	for zzzz@localhost (single-drop); Sun, 22 Sep 2002 14:13:22 +0100 (IST)
> 
> Received: from webnote.net (mail.webnote.net [193.120.211.219]) by
>     dogma.slashnull.org (8.11.6/8.11.6) with ESMTP id g8M9eCC29967 for
>     \<zzzz@jmason.org>; Sun, 22 Sep 2002 10:40:12 +0100
> 
> Received: from smtp.easydns.com (smtp.easydns.com [205.210.42.30]) by
>     webnote.net (8.9.3/8.9.3) with ESMTP id KAA11656 for \<zzzz@example.com>;
>     Sun, 22 Sep 2002 10:40:45 +0100
> 
> From: mortgage_quotes_fast@nationwidemortgage.us
> 
> Received: from mortgages101.net (mail4.mortgages101.net [4.38.36.3]) by
>     smtp.easydns.com (Postfix) with SMTP id 0AE9C2C3C6 for \<zzzz@example.com>;
>     Sun, 22 Sep 2002 05:40:10 -0400 (EDT)
> 
> To: \<zzzz@example.com>
> 
> Subject: Adv: Mortgage Quotes Fast Online, No Cost
> 
> Message-Id: \<20020922094010.0AE9C2C3C6@smtp.easydns.com>
> 
> Date: Sun, 22 Sep 2002 05:40:10 -0400 (EDT)
> 
> Content-Type: text/html
> 
> \<html>
> 
> \<head>
> \<title>Home Page\</title>
> \</head>
> 
> \<body>
> 
> \<p align="center">\<font color="#000000" face="Arial" size="+0">\<b>\<IMG SRC="http://mail4.mortgages101.net/logo.php?id=90&id2=193953">\</p>
> 
> \<p align="center">If this promotion has reached you in error and you would prefer not to
> receive marketing messages from us, please send an email to&nbsp; \<a
> href="mailto:cease-and-desist@mortgages101.net">cease-and-desist@mortgages101.net\</a>
> &nbsp; (all one word, no spaces) giving us the email address in question or call
> 1-888-748-7751 for further assistance.\</p>
> 
> \<p align="center">\<u>Gain access to a\</b>\</font>\<font size="+1" color="#000000"
> face="Arial"> \<i>\<b>Vast Network Of Qualified Lenders at Nationwide Network!\</b>\</i>\</font>\</u>\</p>
> 
> \<p align="center">\<font color="#000000" face="Arial">This is a zero-cost service which
> enables you to shop for a mortgage conveniently from your home computer. &nbsp; Our
> nationwide database will give you access to lenders with a variety of loan programs that
> will work for Excellent, Good, Fair or even Poor Credit! \</font>\<br>
> &nbsp; \<font face="Arial,Helvetica">\<b>\<font color="#000000">We will choose up to 3 mortgage companies
> from our database of&nbsp; registered brokers/lenders.\</font>\</b> \<b>\<font
> face="Arial,Helvetica">Each will contact you to offer you their best rate and terms - at
> no charge.\</font>\</b>\<br>
> &nbsp; \<br>
> &nbsp;\</font>\<b>\<font face="Arial,Helvetica" size="+1" color="#ff0000">You choose the best
> offer and save - \<A HREF="http://mail4.mortgages101.net/point.php?id=90&id2=193953"> Shop here for your next mortgage with just ONE\</font>\<font
> face="Arial,Helvetica">\<font color="#ff0000">\<font size="+1">\<font face="Arial,Helvetica">\</font>
> CLICK - \</A>\</font>\</font>\</b> \<br>
> &nbsp; \<br>
> \</font>\<font face="Arial,Helvetica" size="+0" color="#000000">\<b>Poor or Damaged Credit Is
> Not A Problem!\</b>\</font>\<font face="Arial,Helvetica"> \</p>
> 
> \<ul>
>   \<b>\<font size="+0">\<font color="#cc3300">\<p align="center">Consolidate\</font> &amp; pay
>   off \<font color="#cc3300">high interest bills\</font> for one lower monthly payment!&nbsp;\</font>\</b>
>   \</p>
>   \<p align="center">\<b>\<font size="+0">\<font color="#cc3300">Refinance\</font> (with or
>   without cash out) to a \<font color="#cc3300">low FIXED rate \</font>and payment!\</font>\</b>
>   \</p>
>   \<p align="center">\<b>\<font size="+0">Get money\<font color="#cc3300"> \</font>to cover
>   expenses for \<font color="#cc3300">tuitions\</font>, \<font color="#cc3300">home
>   improvements\</font>, \<font color="#cc3300">a new vehicle or vacations.\</font>\</font>\</b>\</p>
>   \<p align="center">\<b>\<font size="+0">- Talk with up to three of our lenders today!
>  \<A HREF="http://mail4.mortgages101.net/point.php?id=90&id2=193953">   VISIT OUR SITE HERE!   \</A> to get no-cost rate and payment quotes.&nbsp;
>   This service is completely FREE to you!\</font>\</b>\</p>
> \</ul>
> \<b>\<i>\<font size="+1">
> 
> \<p align="center">\</font>\</i>\</b>&nbsp;\</p>
> \</font>
> 
> \<p align="center">\<font face="Arial,Helvetica">If this promotion has reached you in error
> and you do not want to be contacted by us further, \<A HREF="http://mail4.mortgages101.net/optout.php?id=90&id2=193953">click here\</A> and let us know.&nbsp; You
> will not be bothered by us at this email address again.&nbsp; Alternatively, you may send
> an email to \<a href="mailto:cease-and-desist@mortgages101.net">cease-and-desist@mortgages101.net\</a>
> &nbsp;giving us the email address in question for IMMEDIATE attention.&nbsp; Should you
> wish to delete your email address from our mailing list by phone, please call
> 1-888-748-7751 and leave your email address - please spell your email address
> clearly.&nbsp;&nbsp; You may also mail a written request to us at Compliance, NMLN, 3053
> Rancho Vista Blvd. #H-252, Palmdale, CA, 93551.&nbsp; Your request will be honored within
> 24 hours of our receipt of your mail.&nbsp; Failure to exclude yourself from our recurring
> mailer via any of the lawful channels provided means that you have given your consent to
> be included in our mailer.&nbsp; You will continue to receive email as long as you do NOT
> delete yourself from our mailer.&nbsp; Please do not continue to receive unwanted email
> after we have provided you with lawful means to be excluded.&nbsp; We log, date and retain
> ALL delete requests.&nbsp; NO PART OF THIS STATEMENT MAY BE AMENDED OR ELIMINATED.&nbsp;
> Thank you.\<br>
> &nbsp; \</font>\</p>
> \</body>
> \</html>

Processed email:

> adv mortgag quot fast onlin cost home page promot reach error prefer receiv market messag send email word space give email address question assist gain access vast network qualifi lender nationwid network zero cost servic enabl shop mortgag conveni home nationwid databas access lender varieti loan program work excel good fair poor credit choos mortgag compani databas regist broker lender contact offer best rate term charg choos best offer save shop mortgag click poor damag credit problem consolid amp pai high bill lower monthli payment refin cash low fix rate payment monei cover expens tuition home improv new vehicl vacat talk lender todai visit site cost rate payment quot servic complet free promot reach error want contact click let know bother email address altern send email give email address question immedi attent wish delet email address mail list phone leav email address spell email address clearli mail written request complianc nmln rancho vista blvd palmdal request honor hour receipt mail failur exclud recur mailer law channel provid mean given consent includ mailer continu receiv email long delet mailer continu receiv unwant email provid law mean exclud log date retain delet request statement amend elimin thank

# Features and Usage

* Clone this project and install requirements.
```
[niar @ niar] git clone https://gitlab.com/nvvu99/btl-it3190.git
[niar @ niar] cd btl-it3190
[niar @ btl-it3190] virtualenv venv
[niar @ btl-it3190] source ./venv/bin/activate
(venv) [niar @ btl-it3190] pip install -r requirements.txt
```
* Create a python file at current foler
```
(venv) [niar @ btl-it3190] touch test.py
```
* Open this file
* Import all modules
``` python
from preprocessing import *
from spam_classifier import SpamClassifier
from model_selection import train_test_split
from utility import *
```
* Load data set
``` python
easy_ham, hard_ham, spam = load_dataset(data_preprocessed=False) # set to True if you want to load the preprocessed data
```
* Preprocessing
``` python
preprocessed_easy_ham = [message for message in easy_ham]
preprocessed_hard_ham = [message for message in hard_ham]
preprocessed_spam = [message for message in spam]
```
* Train test split
``` python
train_ratio = 0.66
messages = preprocessed_easy_ham + preprocessed_spam
labels = [SpamClassifier.HAM] * len(preprocessed_easy_ham) + \
    [SpamClassifier.SPAM] * len(preprocessed_spam)
x_train, y_train, x_test, y_test = train_test_split(messages, labels, train_ratio=train_ratio)
```
* Training
``` python
classifier = SpamClassifier(x_train, x_test)
```
* Predict a new email
``` python
message = '''NEED Health Insurance? 
 In addition to featuring the largest selection of major medical 
health plans from leading companies, our service also
offers a wide selection of quality dental plans.  You can obtain 
FREE instant quotes, side-by-side comparisons, the best available 
prices, online applications, and a knowledgeable Customer Care 
team to help you find the plan that is right for you.
If you would like more information please email
surefiremarketing@btamail.net.cn?subject=healthinsurance
with "Send me health insurance info" in the body of the email
***************************************************************
If you do not wish to correspond with us, reply to 
surefiremarketing@btamail.net.cn  with remove as your subject. '''

label = classifier.classify(message, data_preprocessed=False) # set to True if the email was preprocessed, set to False if not
if label == SpamClassifier.HAM:
    print('ham')
else:
    print('spam')
```
* Predict a list of email
``` python
predicted = classifier.predict(y_train, data_preprocessed=True)
print(predicted[:20])
```
* Accuracy
``` python
print(accuracy(y_test, predicted))
```
* Confusion matrix
``` python
true_positive, false_negative, false_positive, true_negative = confusion_matrix(y_test, predicted, label=SpamClassifier.HAM)
print(confusion_matrix(y_test, predicted, label=SpamClassifier.HAM))
```

# LICENSE
MIT License.