import os
import re
from gensim.parsing.preprocessing import (
    strip_multiple_whitespaces,
    strip_numeric,
    strip_punctuation,
    strip_short,
    strip_tags,
    stem_text,
    remove_stopwords,
    preprocess_string
)
from email.parser import Parser


def strip_html_symbols(message):
    html_symbols = [
        '&nbsp;',
    ]
    for symbol in html_symbols:
        message = re.sub(symbol, ' ', message)
    return message


def strip_url(message):
    return re.sub(r'http\S+', ' ', message)


def strip_email_address(message):
    return re.sub(r'\S+@\S+', ' ', message)


def strip_email_header(message):
    message = Parser().parsestr(message)
    subject = message.get('subject', '')
    body = get_email_body(message)

    return '\n'.join((subject, body))


def get_email_body(message):
    payloads = message.get_payload()
    if isinstance(payloads, list):
        return '\n'.join([get_email_body(message) for message in payloads])
    elif isinstance(payloads, str):
        return payloads


def data_preprocessing(raw_message):
    FILTERS = [
        strip_email_header,
        str.lower,
        strip_html_symbols,
        strip_tags,
        strip_url,
        strip_email_address,
        strip_punctuation,
        strip_numeric,
        strip_multiple_whitespaces,
        strip_short,
        remove_stopwords,
        stem_text,
    ]

    tokens = preprocess_string(raw_message, filters=FILTERS)
    return ' '.join(tokens)
