from collections import Counter
from math import log
from utility import word_freq, split_by_label
from preprocessing import data_preprocessing


class SpamClassifier:
    HAM = 0
    SPAM = 1

    def __init__(self, messages, labels):
        self.messages_by_labels = split_by_label(messages, labels)
        self.calc_posterior_prob()

    def calc_posterior_prob(self):
        self.word_in_ham_freq = word_freq(
            self.messages_by_labels[SpamClassifier.HAM])
        self.word_in_spam_freq = word_freq(
            self.messages_by_labels[SpamClassifier.SPAM])

        total_words_in_ham = sum(self.word_in_ham_freq.values())
        total_words_in_spam = sum(self.word_in_spam_freq.values())

        words_in_ham = set(self.word_in_ham_freq.keys())
        words_in_spam = set(self.word_in_spam_freq.keys())

        self.words_set = words_in_ham.union(words_in_spam)
        total_unique_words = len(self.words_set)

        self.word_in_ham_prob = Counter()
        self.word_in_spam_prob = Counter()

        for word in self.words_set:
            self.word_in_ham_prob[word] = (
                self.word_in_ham_freq[word] + 1) / (total_words_in_ham + total_unique_words)
            self.word_in_spam_prob[word] = (
                self.word_in_spam_freq[word] + 1) / (total_words_in_spam + total_unique_words)

        total_ham = len(self.messages_by_labels[SpamClassifier.HAM])
        total_spam = len(self.messages_by_labels[SpamClassifier.SPAM])
        self.ham_prob = total_ham / (total_ham + total_spam)
        self.spam_prob = total_spam / (total_ham + total_spam)

    def classify(self, message, data_preprocessed=False):
        if not data_preprocessed:
            message = data_preprocessing(message)
        log_likelihood_ham = log_likelihood_spam = 0.0
        message_words = set(message.split())

        for word in self.words_set:
            if word in message_words:
                log_likelihood_ham += log(self.word_in_ham_prob[word])
                log_likelihood_spam += log(self.word_in_spam_prob[word])

        log_likelihood_ham += log(self.ham_prob)
        log_likelihood_spam += log(self.spam_prob)

        if log_likelihood_spam >= log_likelihood_ham:
            return SpamClassifier.SPAM
        return SpamClassifier.HAM

    def predict(self, messages, data_preprocessed=False):
        result = []
        for message in messages:
            result.append(self.classify(message, data_preprocessed=data_preprocessed))
        return result
