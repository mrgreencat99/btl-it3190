from random import shuffle
from utility import split_by_ratio, split_by_label


def train_test_split(samples, labels, train_ratio):
    samples_by_labels = split_by_label(samples, labels)

    train, test = [], []
    for label in set(labels):
        x_train, x_test = split_by_ratio(
            samples_by_labels[label], ratio=train_ratio)

        y_train = [label] * len(x_train)
        y_test = [label] * len(x_test)

        train.extend(zip(x_train, y_train))
        test.extend(zip(x_test, y_test))

    shuffle(train)
    shuffle(test)

    x_train = [item[0] for item in train]
    y_train = [item[1] for item in train]

    x_test = [item[0] for item in test]
    y_test = [item[1] for item in test]

    return x_train, x_test, y_train, y_test
