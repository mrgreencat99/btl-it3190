from preprocessing import *
from spam_classifier import SpamClassifier
from model_selection import train_test_split
from utility import *


def main():
    # load raw data, set to True if you want to load preprocessed data
    data_preprocessed = False
    easy_ham, hard_ham, spam = load_dataset(
        data_preprocessed=data_preprocessed)

    # messages = easy_ham + spam
    # labels = [SpamClassifier.HAM] * len(easy_ham) + [SpamClassifier.SPAM] * len(spam)

    small_easy_ham, _ = split_by_ratio(easy_ham, 0.05)
    small_spam, _ = split_by_ratio(spam, 0.5)

    messages = easy_ham + spam
    labels = [SpamClassifier.HAM] * (len(easy_ham)) + [SpamClassifier.SPAM] * len(spam)

    train_ratios = [0.4, 0.5, 0.6, 0.7, 0.8]
    accuracies1 = []
    accuracies2 = []
    # for train_ratio in train_ratios:
        # for _ in range(5):
    train_ratio = 0.66
    x_train, x_test, y_train, y_test = train_test_split(
        messages, labels, train_ratio=train_ratio)

    # x_train = [data_preprocessing(message) for message in x_train]
    classifier = SpamClassifier(x_train, y_train)

    predicted = classifier.predict(x_test, data_preprocessed=data_preprocessed)
    accuracies1.append(accuracy(y_test, predicted))
    # predicted = classifier.predict(small_easy_ham, data_preprocessed=data_preprocessed)
    # accuracies2.append(accuracy([0]*len(small_easy_ham), predicted))

    print('{:.2f} {:.3f}'.format(train_ratio, average(accuracies1)))
    print(confusion_matrix(y_test, predicted, SpamClassifier.HAM))


if __name__ == "__main__":
    main()
